(function($) {   
    "use strict";
    $('.slide-3').slick({
        infinite: true,
        speed: 300,
        dots: true,
        navs:false,
        slidesToShow: 3,
        slidesToScroll: 1,
        nav:true,
        margin:15 ,       
        responsive: [
            {
                breakpoint: 1220,
                settings: {
                    slidesToShow: 2,
                    slidesToScroll: 2
                }
            },
            {
                breakpoint: 1210,
                settings: {
                    slidesToShow: 3,
                    slidesToScroll: 3,
                    autoplay: true,
                    autoplaySpeed: 2000
                }
            },
            {
                breakpoint: 992,
                settings: {
                    slidesToShow: 2,
                    slidesToScroll: 2,
                    autoplay: true,
                    autoplaySpeed: 2000
                }
            },
            {
                breakpoint: 991,
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1,
                    autoplay: true,
                    autoplaySpeed: 2000
                }
            },
            {
                breakpoint: 576,
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1,
                    autoplay: true,
                    autoplaySpeed: 2000
                }
            },
            {
                breakpoint: 480,
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1,
                    autoplay: true,
                    autoplaySpeed: 2000
                }
            }
            
        ]
    });

    if ($(window).width() <= 767) {
        console.log("asd");
        $(window).scroll(function() {
            var scroll = $(window).scrollTop();
            if (scroll >= 20) {
                $(".navbar").addClass("darkHeader");
            } else {
                $(".navbar").removeClass("darkHeader");
            }
        });
    }
})(jQuery)